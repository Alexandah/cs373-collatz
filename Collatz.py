#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List
import math

# ------------
# collatz_read
# ------------

def collatz_read (s: str) -> List[int] :
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------

#base case of 1 is tracked in the dict.
#This allows for more elegant cycle_len code,
#since we mustn't check the n==1 independently.
cycle_lens = {1:1}

def cycle_len(n):
    assert n > 0
    
    if n in cycle_lens:
        return cycle_lens[n]

    #optimization cases

    #case n = 2^x where x is an int, then the
    #remaining cycle len is calculable analytically
    x = math.log(n, 2)
    if x.is_integer():
        cl = 1 + int(x)

    elif n % 2 == 0:
        cl =  1 + cycle_len(n//2)
    else:
        cl =  1 + cycle_len(3*n + 1)

    if n not in cycle_lens:
        cycle_lens[n] = cl

    assert cl > 0
    return cl

#interval helper code

#return the interval of overlap between these 2 intervals
#None if no overlap
def interval_intersection(a, b):
    if a is None or b is None:
        return None

    amin, amax = a
    bmin, bmax = b
    assert amin > 0 and bmin > 0
    assert amin <= amax and bmin <= bmax

    highmin = max(amin, bmin)
    lowmax = min(amax, bmax)

    if lowmax < highmin:
        return None
    else:
        return (highmin, lowmax)

#returns True if the interval intersection a,b is a subset of the interval b
#false otherwise
def is_subinterval(a, b):
    intersect = interval_intersection(a,b)
    if intersect is None:
        return False
    if b[0] <= intersect[0] and intersect[1] <= b[1]:
        return True
    return False

#splits an interval by removing an intersecting interval
#returns a list of the remaining 1 to 2 interval(s)
def xor_intervals(a, b):
    new_intervals = []
    intersect = interval_intersection(a,b)
    if intersect is None:
        if a not is None:
            new_intervals.append(a)
        if b not is None:
            new_intervals.append(b)
    else:
        minmin = min(a[0], b[0])
        maxmax = max(a[1], b[1])
        highmin, lowmax = intersect
        new_intervals.append((minmin, highmin))
        new_intervals.append((lowmax, maxmax))
    return new_intervals

#subtracts the intersecting interval for a - b
#returns a list of the remaining 0 to 2 interval(s)
def subtract_intervals(a, b):
    new_intervals = []
    intersect = interval_intersection(a,b)
    if intersect is None:
        new_intervals.append(a)
    elif is_subinterval(a,b):
        pass
    else:
        minmin = min(a[0], b[0])
        maxmax = max(a[1], b[1])
        highmin, lowmax = intersect
        if is_subinterval(b,a):
            new_intervals.append((minmin, highmin))
            new_intervals.append((lowmax, maxmax))
        elif a[1] == maxmax:
            new_intervals.append((lowmax, maxmax))
        else:
            new_intervals.append((minmin, highmin))

    return new_intervals    


#the metacache
class CollatzMetaCache:
    def __init__(self):
        #a collection of interval tuples ordered by smallest starting number  
        self.intervals = []
        #a dict mapping intervals to their corresponding max cycle len
        self.interval_max_len = {}

    def add(self, interval, max_cycle_len):
        self.intervals.append(interval)
        #keep them sorted by end pos
        sorted(self.intervals, key = lambda x : x[1])
        self.interval_max_len[interval] = max_cycle_len

    #return a list of the subintervals of 'a'
    def get_subintervals(self, a):
        subintervals = []
        for i in self.intervals:
            #stop looking if you've passed the range of possible subintervals
            if i[0] > a[1]:
                break
            if is_subinterval(i, a):
                subintervals.append(i)

        return subintervals



def collatz_eval (i: int, j: int) -> int :
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    #first simple implementation w/o metacache
    max_len = 0
    for x in range(i,j+1):
        max_len = max(cycle_len(x), max_len)
    return max_len

# -------------
# collatz_print
# -------------

def collatz_print (w: IO[str], i: int, j: int, v: int) -> None :
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------

def collatz_solve (r: IO[str], w: IO[str]) -> None :
    """
    r a reader
    w a writer
    """
    metacache = CollatzMetaCache()

    for s in r :
        i, j = collatz_read(s)
        #first, check if there are any subintervals of this interval
        #in the metacache
        subintervals = metacache.get_subintervals((i,j))
        #if there are any, use this information to split the task
        #into more manageable parts
        if len(subintervals) != 0:
            #subtract the subintervals from the remaining uncovered
            #intervals until done
            uncovered = [(i,j)]
            for subinterval in subintervals:
                new_uncovered = []
                for leftover in uncovered:
                    new_uncovered += subtract_intervals(leftover, subinterval)
                uncovered = new_uncovered

            #now that we have the remaining uncovered intervals,
            #calculate their max cycle len and record it
            for interval in uncovered:
                metacache.add(interval, collatz_eval(interval[0], interval[1]))

            #finally, loop through all the relevant intervals to compute the max
            all_intervals = []
            all_intervals += subintervals
            all_intervals += uncovered
            maxcl = 0
            for interval in all_intervals:
                maxcl = max(maxcl, metacache.interval_max_len[interval])
            v = maxcl
        else:
            v    = collatz_eval(i, j)
            metacache.add((i,j), v)
            
        collatz_print(w, i, j, v)

# ----
# main
# ----
"""
from sys     import stdin, stdout
if __name__ == "__main__" :
    collatz_solve(stdin, stdout)