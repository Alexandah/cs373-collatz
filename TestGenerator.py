#for randomly generating acceptance tests
import random
from Collatz import collatz_solve

data = open("collatz-tests/Alexandah-RunCollatz.in","w")
#500 tests
for i in range(500):
	start = random.randint(1,999997)
	end = random.randint(start+1, 999999)
	data.write(str(start)+" "+str(end)+"\n")
data.close()

data = open("collatz-tests/Alexandah-RunCollatz.in","r")
results = open("collatz-tests/Alexandah-RunCollatz.out", "w")

collatz_solve(data, results)