# CS373: Software Engineering Collatz Repo

* Name: Todd "Alexander" Davis

* EID: TAD2549

* GitLab ID: Alexandah

* HackerRank ID: talexanderd99

* Git SHA: 95042082886aaed063368adb4fbce9bd322bb71c

* GitLab Pipelines: https://gitlab.com/Alexandah/cs373-collatz/pipelines

* Estimated completion time: 12hrs

* Actual completion time: 20hrs

* Comments: I really got thrown for a loop by the lack of cooperation from my computers
* when I tried to get docker working. I in general had much trouble with trying to get the prerequisite programs
* working for similar reasons which made many of the failures in the pipeline quite meaningless to me because many
* seemed to reflect a problem with the environment as opposed to with my code. Consequently I got desperate and attempted to
* finish the metacache in one go without meaningfully testing each component and unsurprisingly it doesnt work. This was an attempt
* to make up for the problems with using the development software but ironically not having the development software made this harder to do.
* One of the previous commits submitted gets a better score on Hacker rank, so that's what I have kept as my final hackerrank submission.
* For these reasons I do not expect to do well on this project, but hope that the grader may at least show me some mercy, as I really
* did put in a lot of work.
